#!/bin/bash

ENV_DIR="data/env"

if [ -z "$VIRTUAL_ENV" ] ; then
	if [ -d "$ENV_DIR" ]; then
		source "$ENV_DIR/bin/activate"
	else
		echo "Creando virtualenv en '$ENV_DIR' "
		mkdir -p "`dirname "$ENV_DIR"`"
		python3 -m venv "$ENV_DIR"
		source "$ENV_DIR/bin/activate"
		pip install wheel==0.36.2
		pip install -r requirements.txt
	fi
fi

python3 run.py

