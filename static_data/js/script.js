function dqs(selector) {
	return document.querySelector(selector)
}

function dqsa(selector) {
	return document.querySelectorAll(selector)
}

function init() {
	dqs(".loading-message").classList.add("hide")
	dqs("nav").classList.remove("hide")
	var titles = dqsa(".feature-selection [data-feature]")
	for(let title of titles) {
		let feature = title.dataset.feature
		title.addEventListener("click", function() {
			show_feature_report(feature)
		})
	}
	show_feature_report(titles[0].dataset.feature)
}

function show_feature_report (feature) {
	hide_all_feature_reports()
	var feature_report = dqs('.feature-report[data-feature="'+feature+'"]')
	feature_report.classList.remove("hide")
	var button = dqs('a[data-feature="'+feature+'"]')
	button.classList.add("active")
}

function hide_all_feature_reports () {
	var feature_reports = dqsa('.feature-report[data-feature]')
	for(let feature_report of feature_reports) {
		feature_report.classList.add("hide")
	}
	var buttons = dqsa('a[data-feature]')
	for(let button of buttons) {
		button.classList.remove("active")
	}
}

window.addEventListener("load", init)
