import os
import cv2
import sys
import math
import numpy as np

from imageio import imread, imwrite

from src.settings import * 
from src.dataset import Dataset
from src.misc import print_debug

def get_feature_image (image_id, feature):
    thumb_size = Dataset.model_settings['feature_settings'][feature]['thumb_size']
    area_name = Dataset.model_settings['feature_settings'][feature]['area']
    area = Dataset.model_settings['image_areas'][area_name]

    return get_dataset_image_part (image_id, area_name, area, thumb_size)


def get_dataset_image_part (image_id, area_name, area, thumb_size):
    filename = IMAGE_PART_PATTERN.format(image_id=image_id, area_name=area_name, thumb_size=thumb_size)
    if os.path.exists(filename):
        return imread(filename)
    else:
        create_parent_directories(filename)
        orig_filename = DATASET_IMAGE_PATTERN.format(image_id=image_id)
        image_full = imread(orig_filename)
        top = area['top']
        bottom = area['top']+area['height']
        left = area['left']
        right = area['left']+area['width']
        image_part = image_full[top:bottom, left:right]
        scale_factor = 1/2**thumb_size
        #print_debug('        Recorte %s [%d:%d, %d:%d] scale:%f' % (filename, top, bottom, left, right, scale_factor))
        if thumb_size > 0:
            resized = cv2.resize(image_part, None, fx=scale_factor, fy=scale_factor)
            image_part = resized
            imwrite(filename, resized)
        else:
            imwrite(filename, image_part)
        return image_part

def image_part_exists (image_id, area_name, thumb_size):
    filename = IMAGE_PART_PATTERN.format(image_id=image_id, area_name=area_name, thumb_size=thumb_size)
    return os.path.exists(filename)

def create_parent_directories(filename):
    dirname = os.path.dirname(filename)
    if not os.path.exists(dirname):
        try:
            os.makedirs (dirname)
        except:
            pass
 
def get_estimated_bytes_per_sample(feature):
    thumb_size = Dataset.model_settings['feature_settings'][feature]['thumb_size']
    area_name = Dataset.model_settings['feature_settings'][feature]['area']
    area = Dataset.model_settings['image_areas'][area_name]
    dimensions = (math.ceil(area['height'] / 2**thumb_size), math.ceil(area['width'] / 2**thumb_size), 3)
    empty_2d = np.zeros(dimensions, dtype=np.uint8)
    empty_1d = empty_2d.flatten()
    return sys.getsizeof(empty_1d)

   
