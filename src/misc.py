import math
from datetime import datetime
from multiprocessing import Pool, cpu_count

from src.settings import *
from src.dataset import Dataset

RUN_PROCESSES = min(cpu_count(), MAX_PROCESSES or float("inf"))


def print_debug (text):
    if PRINT_DEBUG_ENABLED:
        print("%s %s"%(datetime.now().strftime('%H:%M:%S'),text))


def pllmap (function, params, chunk_size=1):
    pool = Pool(RUN_PROCESSES)
    result = pool.map(function, params, chunk_size)
    pool.close()
    return result

def pllsmap (function, params, chunk_size=1):
    pool = Pool(RUN_PROCESSES)
    result = pool.starmap(function, params, chunk_size)
    pool.close()
    return result

