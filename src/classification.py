import os
import gc
import math
import pickle
from sklearn.tree import DecisionTreeClassifier

from src.settings import * 
from src.dataset import Dataset
from src.misc import print_debug
from src.image import get_feature_image
from src.dimension_reduction import get_model

DEFAULT_ALGORITHM = "dtree"

def get_classifier (feature, dimensions, algorithm=DEFAULT_ALGORITHM):
    classifier = None
    if classifier_exists(feature, dimensions, algorithm):
        classifier = get_existing_classifier (feature, dimensions, algorithm)
    else:
        model = get_model(feature)
        train_data = get_training_data_for_classifier(feature, dimensions, model)
        classifier = train_classifier (feature, dimensions, model, train_data, algorithm)
        save_classifier(classifier, feature, dimensions, algorithm)
    return classifier

def classifier_exists (feature, dimensions, algorithm=DEFAULT_ALGORITHM):
    classifier_path = CLASSIFIER_PATTERN.format(feature=feature, dimensions=dimensions, algorithm=algorithm)
    return os.path.exists (classifier_path)
    
def get_existing_classifier(feature, dimensions, algorithm=DEFAULT_ALGORITHM):
    classifier_path = CLASSIFIER_PATTERN.format(feature=feature, dimensions=dimensions, algorithm=algorithm)
    input_file = open(classifier_path, 'rb')
    classifier = pickle.load(input_file)
    input_file.close()
    return classifier

def save_classifier(classifier, feature, dimensions, algorithm=DEFAULT_ALGORITHM):
    classifier_path = CLASSIFIER_PATTERN.format(feature=feature, dimensions=dimensions, algorithm=algorithm)
    dirname = os.path.dirname(classifier_path)
    if not os.path.exists(dirname):
        os.makedirs (dirname)
    output_file = open(classifier_path, 'wb')
    pickle.dump(classifier, output_file)
    output_file.close()


def train_classifier (feature, dimensions, model, train_data, algorithm=DEFAULT_ALGORITHM):
    print_debug ("Entrenar classifier %s/%d" %(feature, dimensions))
    classifier_input = []
    classifier_expected_output = []
    
    classifier_input = train_data[0]
    classifier_expected_output = train_data[1]
    
    classifier = DecisionTreeClassifier(min_samples_split=5) if algorithm == "dtree" else None
    print_debug ("   Ajustando clasificador")
    classifier.fit (classifier_input, classifier_expected_output)
    return classifier

def get_training_data_for_classifier (feature, dimensions, model):
    limit = math.floor(MAX_CLASSIFICATION_SAMPLES/2)
    cases = {
        'positive': Dataset.get_feature_cases (feature, Dataset.SUBSET_TRAINING, True, limit),
        'negative': Dataset.get_feature_cases (feature, Dataset.SUBSET_TRAINING, False, limit)
    }
    cut = min(len(cases['positive']), len(cases['negative']))

    print_debug ("  %s/%d pos: %d, neg: %d, limit:%d, cut: %d" % (feature, dimensions, len(cases['positive']), len(cases['negative']), limit, cut))
    mixed_ids = []
    for index in range(0, cut):
        for case_type in cases:
            mixed_ids.append(cases[case_type][index])

    train_inputs = [model.transform([get_feature_image (image_id, feature).flatten()])[0][0:dimensions] for image_id in mixed_ids]
    train_outputs = [Dataset.attribs[image_id][feature] for image_id in mixed_ids]
        
    return (train_inputs, train_outputs)
    
