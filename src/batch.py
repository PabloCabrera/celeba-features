import sys
import math
import logging
import functools
import traceback

from src.misc import *
from src.settings import *
from src.image import get_dataset_image_part, image_part_exists
from src.dimension_reduction import model_exists, generate_model, get_model, get_existing_model
from src.classification import train_classifier, get_training_data_for_classifier, save_classifier, get_existing_classifier

class BatchGenerator:

    def run():
        if BATCH_GENERATE_THUMBNAILS:
            BatchGenerator.generate_thumbnails()
        if BATCH_GENERATE_MODELS:
            BatchGenerator.generate_models()
        if BATCH_GENERATE_CLASSIFIERS:
            BatchGenerator.generate_classifiers()

    def generate_thumbnails ():
        for feature in Dataset.model_settings['feature_settings']:
            area_name = Dataset.model_settings['feature_settings'][feature]['area']
            thumb_size = Dataset.model_settings['feature_settings'][feature]['thumb_size']
            BatchGenerator.generate_thumbnails_for (feature, area_name, thumb_size)

    def generate_thumbnails_for (feature, area_name, thumb_size):
        print_debug("Generando thumbnails para %s (%s/%d)"%(feature, area_name, thumb_size))
        area = Dataset.model_settings['image_areas'][area_name]
        limit = math.floor(max(MAX_MODEL_TRAINING_SAMPLES, MAX_VALIDATION_SAMPLES)/2)
        image_ids = set()

        for subset in (Dataset.SUBSET_TRAINING, Dataset.SUBSET_VALIDATION):
            positive_cases = Dataset.get_feature_cases (feature, subset, True, limit)
            negative_cases =  Dataset.get_feature_cases (feature, subset, False, limit)
            cut = min(len(positive_cases), len(negative_cases))
            print_debug("pos: %d, neg: %d, limit: %d, cut: %d" % (len(positive_cases), len(negative_cases), limit, cut))
            image_ids = image_ids.union(positive_cases[0:cut] + negative_cases[0:cut])

        func_generate_image = functools.partial(BatchGenerator.aux_generate_image, area_name, area, thumb_size)
        pllmap(func_generate_image, image_ids, 24)

    def aux_generate_image (an, a, ts, image_id):
        if not image_part_exists(image_id, an, ts):
            get_dataset_image_part (image_id, an, a, ts)

    def generate_models ():
        for feature in Dataset.model_settings['feature_settings']:
            # Esto no se ejecuta en paralelo, ya que usa mucha RAM
            try:
                assert (get_existing_model(feature) is not None)
            except:
                generate_model(feature)

    def generate_classifiers ():
        features = Dataset.model_settings['feature_settings'].keys()
        algorithms = ("dtree",)
        pairs = [(feature, algorithm) for feature in features for algorithm in algorithms]
        pllsmap(BatchGenerator.aux_generate_classifiers, pairs)

    def aux_generate_classifiers(feature, algorithm):
        try:
            remaining = []
            for dimensions in range(1, PCA_MAX_COMPONENTS+1):
                try:
                    get_existing_classifier(feature, dimensions, algorithm)
                except:
                    remaining.append (dimensions)

            if len(remaining) > 0:
                model = get_model(feature)
                training_inputs, training_outputs = get_training_data_for_classifier(feature, PCA_MAX_COMPONENTS, model)
                print_debug("Generar clasificadores para %s/%s/%s" % (feature, remaining, algorithm))

            for dimensions in remaining:
                print(" %s/%d"%(feature, dimensions))
                training_set_partial = (
                    [ component[0:dimensions] for component in training_inputs ],
                    training_outputs,
                )
                classifier = train_classifier (feature, dimensions, model, training_set_partial, algorithm)
                save_classifier(classifier, feature, dimensions, algorithm)
        except Exception as exception:
            print("Exception en worker: %s"%exception)
            logging.exception(exception)
            raise exception


