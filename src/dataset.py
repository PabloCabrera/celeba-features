import csv
import json
from src.settings import *

class Dataset:
    SUBSET_TRAINING=0
    SUBSET_VALIDATION=1
    SUBSET_TEST=2

    model_settings = None
    attribs = None
    subset_partition = None

    def init():
        Dataset.model_settings = Dataset.get_model_settings()
        Dataset.attribs = Dataset.get_attrib_list()
        Dataset.subset_partition = Dataset.get_subset_partition ()

    def get_model_settings():
        '''Devuelve la especificación de los modelos.'''
        file = open (MODEL_SETTINGS_FILE, 'r')
        settings = json.load (file)
        file.close()
        return settings

    def get_attrib_list ():
        with open (DATASET_ATTRIBS_FILE) as file:
            reader = csv.reader(file)
            headers = None
            records = {}
            for row in reader:
                if headers is None:
                    headers = row
                else:
                    image_id = row[0].replace('.jpg', '')
                    record = {}
                    for i in range(1, len(headers)):
                        attrib = headers[i]
                        record[attrib] = (row[i] == '1')
                    records[image_id] = record
            return records

    def get_subset_partition ():
        with open (DATASET_SUBSET_PARTITION_FILE) as file:
            reader = csv.reader(file)
            headers = None
            records = {}
            for row in reader:
                if headers is None:
                    headers = row
                else:
                    image_id = row[0].replace('.jpg', '')
                    records[image_id] = int(row[1])
            return records

    def get_feature_cases (feature, subset=SUBSET_TRAINING, is_positive=True, limit=None):
        cases = []
        count = 0
        for image_id in Dataset.attribs:
            if (
              ((is_positive==None) or (Dataset.attribs[image_id][feature] == is_positive))
              and
              (subset == Dataset.subset_partition[image_id])
              and
              ((limit == None) or (count < limit))
            ):
                cases.append(image_id)
                count +=1
        return cases


    def get_image_size(feature):
        area_name = Dataset.model_settings["feature_settings"][feature]["area"]
        thumb_size = Dataset.model_settings["feature_settings"][feature]["thumb_size"]
        area = Dataset.model_settings["image_areas"][area_name]
        scale = 1/2**thumb_size
        size = (area["height"]*scale, area["width"]*scale)
        return (int(size[0]), int(size[1]), 3)

