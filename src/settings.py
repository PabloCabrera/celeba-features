DATASET_DIR = "data/dataset"
PCA_MODELS_DIR = "data/dimred_models"
MODEL_SETTINGS_FILE = 'static_data/model_settings.json'
DATASET_ATTRIBS_FILE = DATASET_DIR + "/list_attr_celeba.csv"
DATASET_SUBSET_PARTITION_FILE = DATASET_DIR + "/list_eval_partition.csv"
DATASET_IMAGE_PATTERN = DATASET_DIR +  "/img_align_celeba/img_align_celeba/{image_id}.jpg"
IMAGE_PART_PATTERN = "data/image_parts/{area_name}/{thumb_size}/{image_id}.jpg"
CLASSIFIER_PATTERN = "data/classifiers/{algorithm}/{feature}/{dimensions}"
EVALUATION_PATTERN = "data/evaluation/{algorithm}/{feature}.json"
REPORT_DIR = "data"
REPORT_HTML = "evaluation_report.html"
REPORT_IMG_SUBDIR = "img"

MAX_MODEL_TRAINING_SAMPLES = 162770
MAX_CLASSIFICATION_SAMPLES = 162770
MAX_VALIDATION_SAMPLES = 19867

# Cantidad máxima de memoria RAM utilizada para cargar imágenes
MAX_IMAGE_LOAD_RAM = 16 *1024**3  # 16 GiB

# Cantidad de componentes principales máxima
PCA_MAX_COMPONENTS = 32
PCA_BATCH_SIZE = 500

MAX_PROCESSES = None


BATCH_GENERATE_THUMBNAILS = True
BATCH_GENERATE_MODELS = True
BATCH_GENERATE_CLASSIFIERS = True

PRINT_DEBUG_ENABLED = True
RUN_BATCH_GENERATOR = True

try:
    # pylint: disable=import-error, no-name-in-module, unused-wildcard-import, wildcard-import
    from data.local_settings import *
except ModuleNotFoundError:
    pass
