import json
import base64
import numpy as np
from io import BytesIO, TextIOWrapper
from os import path, makedirs
from matplotlib import pyplot

from src.settings import *
from src.dataset import Dataset
from src.image import get_feature_image
from src.dimension_reduction import get_model, get_reducted_image, generate_image


def generate_report():
    Dataset.init()
    evaluation_results = get_evaluation_results()
    if not path.exists(REPORT_DIR):
        makedirs(REPORT_DIR)
    out_file = open(path.join(REPORT_DIR, REPORT_HTML), 'w')
    write_report_header (out_file)
    out_file.write ('<div class="loading-message">Cargando datos. Esto puede demorar varios segundos...</div>\n')
    out_file.write ('<nav class="feature-selection hide">\n')
    for feature in evaluation_results:
        write_feature_link (feature, evaluation_results[feature], out_file)
    out_file.write ('</nav>\n')
    for feature in evaluation_results:
        write_feature_report (feature, evaluation_results[feature], out_file)
    write_report_tail (out_file)
    out_file.close()

def get_evaluation_results(algorithm="dtree"):
    results = {}
    for feature in Dataset.model_settings['feature_settings']:
        filename = EVALUATION_PATTERN.format(feature=feature, algorithm=algorithm)
        if path.exists(filename):
            file = open(filename, 'r')
            results[feature] = json.load(file)
            file.close()
    return results

def write_feature_link (feature, evaluation_data, outfile):
    best_dimension = get_best_dimension(evaluation_data)
    best_accuracy_p = evaluation_data[best_dimension]['accuracy']
    model = get_model(feature)
    outfile.write ("<a data-feature='%s'>%s %.3f</a>\n"%(feature, feature, float(best_accuracy_p)))
    
def write_feature_report (feature, evaluation_data, outfile):
    best_dimension = get_best_dimension(evaluation_data)
    best_accuracy_p = evaluation_data[best_dimension]['accuracy']
    model = get_model(feature)
    outfile.write ('<div class="feature-report hide" data-feature="%s">\n'%feature)
    outfile.write ("<h2 data-feature='feature-%s'>%s (%.3f)</h2>\n"%(feature, feature, float(best_accuracy_p)))
    outfile.write ('<div class="flex-vertical">\n')
    outfile.write ('<div class="plot-container">\n')
    outfile.write ('<header>Accuracy %s</header>\n'%feature)
    plot_accuracy (feature, evaluation_data, outfile)
    outfile.write ('</div>\n')
    outfile.write ('<div class="plot-container">\n')
    outfile.write ('<header>Mejor accuracy ponderated obtenida con %d dimensiones: %.3f</header>\n'%(int(best_dimension), float(best_accuracy_p)))
    plot_distribution (feature, evaluation_data[best_dimension], outfile)
    outfile.write ('</div>\n')
    outfile.write ('</div><!--.flex-vertical-->\n')
    outfile.write ('<div class="flex-vertical text-center">\n')
    for params in (
       ('tp', 'Verdaderos positivos', 'positive', 'positive'),
       ('fn', 'Falsos negativos', 'positive', 'negative'),
       ('tn', 'Verdaderos negativos', 'negative', 'negative'),
       ('fp', 'Falsos positivos', 'negative', 'positive')
    ):
        plot_samples(evaluation_data[best_dimension], params, feature, model, int(best_dimension), outfile)
    outfile.write ('</div>\n')
    outfile.write ('</div>\n')

def get_best_dimension(data):
    best_accuracy_p = None
    best_dimension = None
    for dimension in data:
        if best_accuracy_p is None or data[dimension]['accuracy_ponderated'] > best_accuracy_p:
            best_accuracy_p = data[dimension]['accuracy_ponderated']
            best_dimension = dimension
    return best_dimension


def plot_accuracy (feature, evaluation_data, outfile):
    figure = get_accuracy_plot(feature, evaluation_data)
    outfile.write('<div class="plot-accuracy">')
    figure_to_html(figure, outfile, "svg")
    outfile.write('</div>')
    pyplot.close(figure)
    pyplot.cla()
    pyplot.clf()

def get_accuracy_plot (feature, evaluation_data):
    figure = pyplot.figure(figsize=(9,4.5))
    for measure, legend, color, linestyle in (
        ('accuracy', 'Accuracy', '#888888', 'dotted'),
        ('accuracy_ponderated', 'Accuracy ponderada', '#4444ff', 'solid'),
        ('accuracy_positive_cases', 'Accuracy casos positivos', '#22aa22', 'dotted'),
        ('accuracy_negative_cases', 'Accuracy casos negativos', '#ff4444', 'dotted')
    ):
        x=[int(dimension) for dimension in evaluation_data]
        y=[evaluation_data[dimension][measure] for dimension in evaluation_data]
        pyplot.plot(x, y, color=color, linestyle=linestyle, label=legend)
    pyplot.xticks(ticks=range(1, 1+len(evaluation_data)))
    pyplot.gca().xaxis.grid(True, which='both')
    pyplot.legend()
    return figure

def plot_distribution (feature, data, outfile):
    figure = get_distribution_plot(feature, data)
    outfile.write('<div class="plot-distribution">')
    figure_to_html(figure, outfile, "svg")
    outfile.write('</div>')
    pyplot.close(figure)
    pyplot.cla()
    pyplot.clf()

def get_distribution_plot(feature, data):
    figure, axis = pyplot.subplots(figsize=(5,3))

    # Outer
    sizes_outer = (data['true_negative'],data['fake_positive'],data['fake_negative'],data['true_positive'])

    # Inner
    sizes_inner = (data['true_negative']+data['fake_positive'], data['true_positive']+data['fake_negative'])
    kwargs_inner = {
        'labels': ('Negatives', 'Positives'),
        'colors': ('#f44336','#4CAF50'),
        'wedgeprops': {'width': 0.7, 'edgecolor': 'w'},
        'startangle': 90, 'radius': 1, 'autopct': '%1.1f%%',
        'textprops': {'fontsize': 'small'},
        'pctdistance': 0.4, 'labeldistance': 0.5
    }
    axis.pie(sizes_inner, **kwargs_inner)

    kwargs_outer = {
        'labels': ('True\nnegatives', 'Fake\npositives', 'Fake\nnegatives', 'True\npositives'),
        'colors': ('#f44336','#4CAF50','#f44336','#4CAF50'),
        'wedgeprops': {'width': 0.3, 'edgecolor': 'w'},
        'startangle': 90, 'radius': 1, 'autopct': '%1.1f%%',
        'textprops': {'fontsize': 'small'},
        'pctdistance': 0.8, 'labeldistance': 1
    }
    axis.pie(sizes_outer, **kwargs_outer)

    axis.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    return figure

def figure_to_html(figure, outfile, format="svg"):
    buffer = BytesIO()
    figure.savefig(buffer, format=format)
    buffer.seek(1)
    if format == "svg":
        sbuffer = TextIOWrapper(buffer, encoding="utf-8")
        header = True
        for line in sbuffer:
            if line.startswith('<svg'):
                header = False
            if not header:
                outfile.write(line)
        sbuffer.close()
    elif format == "jpeg":
        outfile.write('<img src="data:image/jpeg;base64,')
        outfile.write(base64.b64encode(buffer.getvalue()).decode())
        outfile.write('">\n')
    buffer.close()

def plot_samples(evaluation_data, params, feature, model, dimensions, outfile):
    image_ids = evaluation_data["samples_%s" % params[0]]
    percentages = get_percentages(evaluation_data)
    title = params[1]
    value = params[2]
    predicted = params[3]
    outfile.write ('<div class="sample-group">\n')
    outfile.write ('<header class="margin-8">%s (%d %%)</header>\n'% (title, percentages[params[0]]))
    outfile.write ('<div class="flex-vertical align-center justify-center small">\n')
    outfile.write (' <span class="classification-indicator %s">%s</span>\n'%(value, value))
    outfile.write (' <div class="margin-8"> classified<br>as </div>\n')
    outfile.write (' <span class="classification-indicator %s">%s</span>\n'%(predicted, predicted))
    outfile.write ('</div>\n')


    figure, axis = pyplot.subplots(len(image_ids), 2, frameon=False, figsize=(3,7))
    row=0
    for image_id in image_ids:
        try:
            image = get_feature_image (image_id, feature)
            reducted = get_reducted_image (image_id, feature, dimensions, model)
            reconstructed_image = generate_image(reducted, model, dimensions, image.shape)
            axis[row,0].axis('off')
            axis[row,0].imshow(image)
            axis[row,1].axis('off')
            axis[row,1].imshow(np.uint8(reconstructed_image))
            row += 1
        except:
            pass
    figure_to_html(figure, outfile, 'jpeg')
    outfile.write ('</div>\n')
    pyplot.close(figure)

def get_percentages(data):
    total = data["true_positive"] + data["true_negative"] + data["fake_positive"] + data["fake_negative"]
    return {
        "tp": 100 * data["true_positive"]/total,
        "tn": 100 * data["true_negative"]/total,
        "fp": 100 * data["fake_positive"]/total,
        "fn": 100 * data["fake_negative"]/total,
    }


def write_report_header (outfile):
    outfile.write('''
<!DOCTYPE HTML>\n
<head>
    <title>Reporte</title>
    <link rel="stylesheet" type="text/css" href="../static_data/css/style.css">
    <script src="../static_data/js/script.js"></script>
</head>

<body>
''')

def write_report_tail(outfile):
    outfile.write('''
</body>
''')
