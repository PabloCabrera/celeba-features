import gc

import src.misc as misc
import src.dimension_reduction as dr
import src.evaluation as ev
from src.settings import * 
from src.dataset import Dataset
from src.batch import BatchGenerator

def main():
    ''' --- '''
    misc.print_debug('Cargando configuración')
    Dataset.init()

    if (RUN_BATCH_GENERATOR):
        BatchGenerator.run()
    
    for feature in Dataset.model_settings['feature_settings']:
        model = dr.get_model (feature)
        if model is not None:
            ev.evaluate_model(model, feature)
        gc.collect() # Garbage collector
        
