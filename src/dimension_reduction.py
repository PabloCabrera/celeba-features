import gc
import os
import math
import pickle
import functools
import numpy as np
from sklearn.decomposition import IncrementalPCA

from src.settings import *
from src.dataset import Dataset
from src.misc import print_debug, pllmap
from src.image import get_estimated_bytes_per_sample, get_feature_image



def get_model (feature):
    '''Devuelve el modelo  para la feature.'''
    if model_exists (feature):
        return get_existing_model (feature)
    else:
        return generate_model (feature)


def model_exists (feature):
    '''Devuelve true o false según el modelo exista o no.'''
    return os.path.exists(os.path.join(PCA_MODELS_DIR, feature))


def get_existing_model (feature):
    '''Devuelve el modelo previamente generado para la feature. Si no fue generado aún devuelve None.'''
    model = None
    if model_exists (feature):
        input_file = open(os.path.join(PCA_MODELS_DIR, feature), 'rb')
        model = pickle.load(input_file)
        input_file.close()

    return model


def generate_model (feature):
    '''Genera el modelo para la feature y lo devuelve.'''
    print_debug('Generando modelo para feature %s' % feature)
    training_data = get_training_data_for_model (feature)
    
    print_debug('Ajustando modelo para feature %s' % feature)
    model = IncrementalPCA(PCA_MAX_COMPONENTS, batch_size=PCA_BATCH_SIZE)
    model.fit (training_data)
    model_save (model, feature)
    return model


def model_save (model, feature):
    ''' Guardar modelo en un archivo '''
    
    if not os.path.exists(PCA_MODELS_DIR):
        os.mkdir (PCA_MODELS_DIR)
    output_file = open(os.path.join(PCA_MODELS_DIR, feature), 'wb')
    pickle.dump(model, output_file)
    output_file.close()

def get_reducted_image (image_id, feature, dimensions, model):
    return model.transform(
        [get_feature_image (image_id, feature).flatten()]
    )[0][0:dimensions]


def get_training_data_for_model(feature):
    estimated_bytes_per_sample = get_estimated_bytes_per_sample (feature)
    max_samples = math.floor(MAX_IMAGE_LOAD_RAM/estimated_bytes_per_sample)
    print_debug ("   %s, máximo de imágenes a cargar: %d" % (feature, max_samples))
    positive_cases = Dataset.get_feature_cases (feature, Dataset.SUBSET_TRAINING, True, math.floor(MAX_MODEL_TRAINING_SAMPLES/2))
    negative_cases = Dataset.get_feature_cases (feature, Dataset.SUBSET_TRAINING, False, math.floor(MAX_MODEL_TRAINING_SAMPLES/2))
    train_samples_per_case = min(len(positive_cases), len(negative_cases), math.floor(max_samples/2), )
    print_debug ("   POSITIVOS: %d, NEGATIVOS: %d" % (len(positive_cases), len(negative_cases)))
    print_debug ("   Se generará el modelo con %d casos positivos y %d casos negativos" % (train_samples_per_case, train_samples_per_case))

    mixed_ids = []
    for index in range(0, train_samples_per_case):
        mixed_ids.append(positive_cases[index])
        mixed_ids.append(negative_cases[index])

    pllfunc = functools.partial(get_feature_image_inv, feature)
    train_data = pllmap(pllfunc, mixed_ids, 50)

    return np.array(train_data)


def get_feature_image_inv (feature, image_id):
    return get_feature_image(image_id, feature).flatten()

def generate_image (principal_components, model, dimensions, shape):
        vector_head = principal_components
        vector_tail = np.zeros(PCA_MAX_COMPONENTS - len(vector_head))
        filled_vector = np.append(vector_head, vector_tail)
        generated_image = model.inverse_transform([filled_vector])[0]
        generated_image[generated_image < 0] = 0
        generated_image[generated_image > 255] = 255
        generated_image = np.uint8(generated_image).reshape(shape)
        return generated_image
