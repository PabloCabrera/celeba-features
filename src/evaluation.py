import os
import json

from src.settings import *
from src.dataset import Dataset
from src.misc import print_debug
from src.image import get_feature_image
from src.classification import get_classifier
from src.dimension_reduction import get_reducted_image

def evaluate_model (model, feature):
    evaluation_results = {}
    validation_set_full = get_validation_data (feature, PCA_MAX_COMPONENTS, model)
    for algorithm in ("dtree",):
        for dimensions in range(1, PCA_MAX_COMPONENTS+1):
            classifier = get_classifier(feature, dimensions)
            validation_set_reduced = reduce_validation_set (validation_set_full, dimensions)
            print_debug ("Evaluando %s/%d" %(feature, dimensions))
            evaluation_results[dimensions] = get_evaluation_result (classifier, validation_set_reduced)
        save_evaluation_results(evaluation_results, feature, algorithm)
    return evaluation_results

def get_validation_data (feature, dimensions, model):
    inputs = []
    expected_outputs = []
    image_ids = Dataset.get_feature_cases (feature, Dataset.SUBSET_VALIDATION, None, MAX_VALIDATION_SAMPLES)
    for image_id in image_ids:
        inputs.append(get_reducted_image(image_id, feature, dimensions, model))
        expected_outputs.append(Dataset.attribs[image_id][feature])
    
    return {'inputs':inputs, 'expected_outputs': expected_outputs, 'image_ids': image_ids}

def reduce_validation_set (data, dimensions):
    reduced = {'inputs':[], 'expected_outputs': data['expected_outputs'], 'image_ids': data['image_ids']}
    for index in range(0, len(data['inputs'])):
        reduced['inputs'].append(data['inputs'][index][0:dimensions])
    return reduced


def get_evaluation_result (classifier, validation_set):
    MAX_SAMPLES=5
    cf = { # Confusion Matrix
        'true_positive': 0, 'true_negative': 0,
        'fake_positive': 0, 'fake_negative': 0,
        'samples_tp':[], 'samples_tn':[],
        'samples_fp':[], 'samples_fn':[],
    }
    outputs = classifier.predict(validation_set['inputs'])
    expected = validation_set['expected_outputs']
    image_ids = validation_set['image_ids']

    for i in range(0, len(outputs)):
        if outputs[i] and expected[i]:
            cf['true_positive'] +=1
            if (len(cf['samples_tp']) < MAX_SAMPLES):
                cf['samples_tp'].append(image_ids[i])
        elif not outputs[i] and not expected[i]:
            cf['true_negative'] +=1
            if (len(cf['samples_tn']) < MAX_SAMPLES):
                cf['samples_tn'].append(image_ids[i])
        elif outputs[i] and not expected[i]:
            cf['fake_positive'] +=1
            if (len(cf['samples_fp']) < MAX_SAMPLES):
                cf['samples_fp'].append(image_ids[i])
        elif not outputs[i] and expected[i]:
            cf['fake_negative'] +=1
            if (len(cf['samples_fn']) < MAX_SAMPLES):
                cf['samples_fn'].append(image_ids[i])

    cf['accuracy'] = (cf['true_positive'] + cf['true_negative']) / (cf['true_positive'] + cf['true_negative'] + cf['fake_positive'] + cf['fake_negative'])
    cf['accuracy_positive_cases'] = (cf['true_positive']) / (cf['true_positive'] + cf['fake_negative']) if cf['true_positive'] + cf['fake_negative'] > 0 else 0
    cf['accuracy_negative_cases'] = (cf['true_negative']) / (cf['true_negative'] + cf['fake_positive']) if cf['true_negative'] + cf['fake_positive'] > 0 else 0
    cf['accuracy_ponderated'] = (cf['accuracy_positive_cases'] + cf['accuracy_negative_cases']) /2
    print(cf)
    print_debug (" -> ACCURACY: %.3f" %(cf['accuracy'],))
    return cf
            
def save_evaluation_results(results, feature, algorithm):
    out_path = EVALUATION_PATTERN.format(feature=feature, algorithm=algorithm)
    dirname = os.path.dirname(out_path)
    if not os.path.exists (dirname):
        os.makedirs (dirname)
    output_file = open(out_path, 'w')
    json.dump(results, output_file)
    output_file.close()



