#!/usr/bin/env python3

from src.mexec import main
from src.report import generate_report

main()
generate_report()
